const categoryRoute = require('./Category.route');
const productRouter = require('./Product.route');
const providerRoute = require('./Provider.route')
const imageRoute = require('./image.route');
const customerRoute = require('./Customer.route');
const orderRoute = require('./Order.route');
const adminRoute = require('./Admin.route');


function route(app) {
    app.use('/api/provider', providerRoute);
    app.use('/api/category', categoryRoute);
    app.use('/api/product', productRouter);
    app.use('/image', imageRoute);
    app.use('/api/customer', customerRoute);
    app.use('/api/order', orderRoute);
    app.use('/api/admin', adminRoute);
}

module.exports = route;