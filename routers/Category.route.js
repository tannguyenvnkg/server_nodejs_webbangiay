const express = require('express');
const router = express.Router();

const CategoryController = require('../app/controller/CategoryController');

router.get('/getAllCategory', CategoryController.getAllCategory);
router.post('/addCategory', CategoryController.addCategory);
router.put('/updateCategory', CategoryController.updateCategory);
router.get('/',CategoryController.index);

module.exports = router;