const express = require('express');
const router = express.Router();

const AdminController = require('../app/controller/AdminController');

router.get('/', AdminController.index);
router.post('/registerAdmin',AdminController.postRegisterAdmin);
router.put('/lockAdminById',AdminController.putLockAdminById);
router.put('/updateAdminById',AdminController.putUpdateAdminById);
router.get('/getAllAdmin', AdminController.getAllAdmin);
router.get('/getAdminByUsername',AdminController.getAdminByUsername);
router.post('/loginAdmin',AdminController.loginAdmin);
module.exports = router;