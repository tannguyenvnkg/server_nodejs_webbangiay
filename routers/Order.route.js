const express = require('express');
const router = express.Router();

const OrderController = require('../app/controller/OrderController');

router.get('/', OrderController.index);
router.get('/getAllOrder', OrderController.getAllOrder);
router.get('/getOrderById', OrderController.getOrderById);
router.get('/getOrderByCustomerId', OrderController.getOrderByCustomerEmail);
router.get('/getOrderBeingDelivered', OrderController.getOrderBeingDelivered);
router.get('/getOrderDeliverySuccess', OrderController.getOrderDeliverySuccess);
router.get('/getOrderDeliveryCancel', OrderController.getOrderDeliveryCancel);
router.post('/addOrder', OrderController.checkOrder, OrderController.addOrder);
router.put('/deliverySuccess', OrderController.deliverySuccess);
router.put('/deliveryCancel', OrderController.deliveryCancel);

module.exports = router;