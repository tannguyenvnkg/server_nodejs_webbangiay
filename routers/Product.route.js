const express = require('express');
const router = express.Router();

const ProductController = require('../app/controller/ProductController');

const authorization = require('../util/authenToken');

const {uploadProductImage} = require('./Upload.multer');

// router.get('/getAllProduct', authorization.authenTokenClient, ProductController.getAllProduct);
router.get('/getAllProduct', ProductController.getAllProduct);
router.get('/getProductByCategoryID', ProductController.getProductByCategoryID);
router.get('/getProductByProviderID', ProductController.getProductByProviderID);
// router.put('/lu', ProductController.hamlu);
router.post('/addProduct', uploadProductImage.single('image'), ProductController.addProduct);
router.post('/addSizeProduct', ProductController.addDetailsProduct);
router.put('/updateProduct', ProductController.updateProduct);
router.put('/updateImageProduct', uploadProductImage.single('image'), ProductController.updateImageProduct);
router.use('/',ProductController.index);

module.exports = router;