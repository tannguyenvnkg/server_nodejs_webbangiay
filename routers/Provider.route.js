const express = require('express');
const router = express.Router();

const providerController = require('../app/controller/ProviderController');

router.get('/getAllProvider',providerController.getAllProvider);
router.post('/addProvider',providerController.addProvider);
router.put('/updateProvider',providerController.updateProvider);
router.get('/',providerController.index);

module.exports = router;