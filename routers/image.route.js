const express = require('express');
const router = express.Router();

const imageProductController = require('../app/controller/Image/ImageProductController');

router.use('/product/:imageName', imageProductController.display);

router.use('/', imageProductController.index);

module.exports = router;