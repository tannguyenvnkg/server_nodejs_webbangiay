const express = require('express');
const router = express.Router();

const CustomerController = require('../app/controller/CustomerController');

router.get('/', CustomerController.index);
router.get('/getAllCustomer', CustomerController.getAllCustomer);
router.get('/getCustomerByEmail', CustomerController.getCustomerByEmail);
router.post('/loginCustomer', CustomerController.login);
router.post('/registerCustomer', CustomerController.register);
router.post('/checkResetCode', CustomerController.checkResetCode);
router.post('/receiveOTP', CustomerController.receiveOTP);
router.put('/resetPassword', CustomerController.resetPassword);
router.put('/updateCustomer', CustomerController.updateCustomer);
router.put('/changePassword',CustomerController.changePassword);

module.exports = router;