const multer = require('multer');
const path = require('path');

//===============================================================================
//upload image product
var storageImageProduct = multer.diskStorage ({
    destination: function (req, file, cb) {
        const pathProductImage = path.join(__dirname, '../','picture','product','/');
        cb(null, pathProductImage)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
});

var uploadProductImage = multer({ storage: storageImageProduct});



module.exports = {uploadProductImage}