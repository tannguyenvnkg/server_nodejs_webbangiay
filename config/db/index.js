const mongoose = require('mongoose');

async function connect() {
    try {
        await mongoose.connect('mongodb://localhost:27017/ShoesStoreDB', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log('Connect successfully!!!!');
    } catch (error) {
        console.log('failure!!');
        console.log(error);
    }
}

module.exports = {connect};