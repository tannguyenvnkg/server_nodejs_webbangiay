const express = require('express');
//const bodyParser = require ('body-parser');
const cors = require ('cors');
const dotenv = require('dotenv');

const db = require('./config/db/index');

db.connect();

const route = require('./routers/index');

const app = express();

dotenv.config();

const PORT = process.env.port;
// const PORT = process.env.port || 5000;
// const PORT = 5000;

app.use(express.urlencoded({ extended: true}));
app.use(express.json());
app.use(cors());

route(app);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});