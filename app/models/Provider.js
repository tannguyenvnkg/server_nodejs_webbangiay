const mongoose = require('mongoose');
const ProviderSchema = require('./schema/ProviderModel');

module.exports = mongoose.model('Provider',ProviderSchema);