const mongoose = require('mongoose');
const OrderSchema = require('./schema/OrderModel');

module.exports = mongoose.model('Order', OrderSchema);