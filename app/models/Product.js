const mongoose = require('mongoose');

const ProductSchema = require('./schema/ProductModel')

module.exports = mongoose.model('Product', ProductSchema);