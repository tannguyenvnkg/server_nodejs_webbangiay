const mongoose = require('mongoose');
const AdminSchema = require('./schema/AdminModel');

module.exports = mongoose.model('Admin',AdminSchema);