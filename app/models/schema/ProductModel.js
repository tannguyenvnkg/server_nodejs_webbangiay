const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

const Category = require("./CategoryModel");
const Provider = require("./ProviderModel");

const detailsSchema = new Schema({
  sizeId: {
    type: mongoose.Schema.Types.ObjectId,
    index: true,
    required: true,
    auto: true,
  },
  size: Number,
  quantityInStock: Number,
}, {
  versionKey: false,
  _id: false
});

const detailSchema = new Schema({
  sizeId: {
    type: mongoose.Schema.Types.ObjectId,
    index: true,
    required: true,
    auto: true,
  },
  size: Number,
  amount: Number,
}, {
  versionKey: false,
  _id: false
});

const productSchema = new Schema({
  productName: String,
  imageProduct: String,
  price: Number,
  category: Category,
  provider: Provider,
  details: [detailsSchema],
  detail: detailSchema,
  status: Boolean,
}, {
  timestamps: true,
  versionKey: false,
});

module.exports = productSchema;