const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const providerSchema = new Schema({
    providerName : String,
    address : String,
    phone : String,
}, 
    {
        versionKey: false
    }
);
module.exports = providerSchema;