const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    categoryName: String, 
  },
  {
    versionKey: false,
  }
);
module.exports = categorySchema;
