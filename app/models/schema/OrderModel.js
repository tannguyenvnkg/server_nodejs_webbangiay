const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Customer = require('./CustomerModel');

const orderSchema = new Schema(
    {
        nameCustomer: String,
        emailCustomer: String,
        phoneCustomer: String,
        paid: Boolean,
        deliveryStatus: Boolean,
        orderDate: Date,
        deliveryDate: Date,
        totalMoney: Number,
        cart: Array,
        addressCustomer: String
    },
    {
        versionKey: false,
    }
)

module.exports = orderSchema;
