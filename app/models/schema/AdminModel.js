const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AdminSchema = new Schema(
    {
        userName: String,
        password: String,
        fullName: String,
        role: Number,
        status: Boolean,
    }, 
    {
        versionKey: false,
    }
);

module.exports = AdminSchema;