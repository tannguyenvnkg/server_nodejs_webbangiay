const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const customerSchema = new Schema(
    {
        customerName: String,
        phone: String,
        birthday: Date,
        address: String,
        email: String,
        password: String,
        resetCode: String
    },
    {
        versionKey: false,
    }
);

module.exports = customerSchema;
