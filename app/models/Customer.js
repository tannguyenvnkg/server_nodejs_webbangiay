const mongoose = require('mongoose');
const CustomerSchema = require('./schema/CustomerModel');

module.exports = mongoose.model('Customer', CustomerSchema);