const mongoose = require('mongoose');
const CategorySchema = require('./schema/CategoryModel');

module.exports = mongoose.model('Category',CategorySchema);