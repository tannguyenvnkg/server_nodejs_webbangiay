const Category = require("../models/Category");
const Product = require("../models/Product");

class CategoryController {
    //[GET] api/category/getAllCategory
    async getAllCategory(req, res) {
        try {
            const category = await Category.find({});
            res.json({
                error: false,
                message: "Danh sách tất cả các thể loại",
                data: category,
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] api/category/addCategory
    async addCategory(req, res) {
        try {
            const categoryName = req.body.categoryName.trim();
            if (!categoryName) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin",
                });
            } else {
                const newCategory = req.body;
                const category = new Category(newCategory);
                await category.save();
                res.json({
                    error: false,
                    message: `Đã thêm thể loại ${category.categoryName} thành công`,
                });
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[PUT] /category/updateCategory
    async updateCategory(req, res) {
        try {
            const categoryId = req.body._id.trim();
            const categoryName = req.body.categoryName.trim();
            // console.log(categoryId);
            if (!categoryId || !categoryName) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin",
                });
            } else {
                let category = await Category.findOne({
                    _id: categoryId
                });
                if (category) {
                    const updateCategory = req.body;

                    category = await Category.findOneAndUpdate({
                            _id: updateCategory._id
                        },
                        updateCategory, {
                            new: true
                        }
                    );
                    await Product.updateMany({'category._id': categoryId}, {"$set": {category: updateCategory}});
                    res.json({
                        error: false,
                        message: `Đã sửa thể loại ${category.categoryName} thành công`,
                    });
                } else {
                    res.json({
                        error: true,
                        message: "Thể loại không tồn tại",
                    });
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/category/
    async index(req, res) {
        res.json({
            error: true,
            messenger: "nothing happend",
        });
    }
}

module.exports = new CategoryController();