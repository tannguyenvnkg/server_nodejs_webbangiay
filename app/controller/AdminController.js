
const req = require("express/lib/request");
const { get } = require("express/lib/response");
const res = require("express/lib/response");
const Admin = require("../models/Admin");
const { post } = require("../models/schema/AdminModel");


class AdminController {

    // //[GET] api/admin/getAllAdmin
    // async getAllAdmin(req, res) {

    // }

    //[GET] api/admin/
    async index(req, res) {
        res.json({
            error: true,
            message: "Nothing"
        })
    }

    //[POST]
    // tạo 1 admin 
    // so sánh newUser điền vào có trùng với user có trong db
    // nếu trung xuất ra user đã tồn tại
    // nếu ko trùng thì add newUser vào db, xuất ra là thêm user thành công
    //role: 1, 2
    //[POST] /api/admin/registerAdmin
    async postRegisterAdmin(req,res){
        try {
            const AdminNewUser =req.body.userName.trim();
            const AdminPass =req.body.password;
            const AdminFullName =req.body.fullName;
            if(!AdminNewUser || !AdminPass || !AdminFullName){
                res.json({error: true, message: "Vui lòng điền đầy đủ thông tin"});
            }
            const checkAdminNewUser =await Admin.findOne({userName: AdminNewUser});
            if(checkAdminNewUser){
                res.json({error: true, message: "Tên tài khoản đã tồn tại"});
            }else{
                const NewAdmin =new Admin({userName: AdminNewUser,password: AdminPass,fullName: AdminFullName
                    ,role: 2, status:true});
                await NewAdmin.save();
                res.json({error: false, message: "Đăng ký thành công"});
            }
        } catch (err) {
            console.log(err.message);
        }
    }
    //[PUT]
    // khóa tài khoản admin, // admin role : 1 mới có quyền deactive role 2
    //[PUT] /api/admin/lockAdminById
    async putLockAdminById(req, res){
        try {
            const AdminId = req.body.adminId.trim();
            const LockAdminId =req.body.lockAdminId.trim();
            if(!AdminId || !LockAdminId){
                res.json({error: true, message: "Vui lòng điền đầy đủ thông tin"});
            }else{
                const roleAdmin = await Admin.findById(AdminId);
                const LockAdmin = await Admin.findById(LockAdminId);
                if(!roleAdmin || !LockAdmin){
                    res.json({error: true, message:"Id Không tồn tại"});
                }else{
                    if(roleAdmin.role== 1 && LockAdmin.role ==2){
                        await Admin.findOneAndUpdate({LockAdminId,status: false});
                        res.json({error: false, message: "Khóa thành công tài khoản "+LockAdmin.userName});
                    }else{
                        res.json({error: true, message: "Tài khoản bạn không có quyền làm việc này"});
                    }
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }
    //[PUT]
    // sửa thông tin admin// chỉ cho admin quyền role 1 sửa thông tin
    //[PUT] /api/admin/updateAdminById
    async putUpdateAdminById(req,res){
        try {
            const AdminId = req.body.adminId.trim();
            const UpdateAdminId =req.body.updateAdminId.trim();
            const AdminFullName =req.body.fullName.trim();
            const AdminRole =req.body.role;
            const AdminStatus=req.body.status;
            if(!AdminId || !UpdateAdminId || !AdminFullName || !AdminRole || typeof AdminStatus !== 'boolean'){
                // console.log(AdminId + UpdateAdminId + AdminFullName + AdminRole + AdminStatus);
                res.json({error: true, message:"Vui lòng điền đầy đủ thông tin"});
            }else{
                if(AdminRole <=0 || AdminRole > 2){
                    res.json({error: true, message:"Role không hợp lệ cho chỉ phép 1 và 2"});
                }else{
                    const roleAdmin =await Admin.findById(AdminId);
                    const UpdateAdmin =await Admin.findById(UpdateAdminId);
                    if(!roleAdmin || !UpdateAdmin){
                        res.json({error: true, message:"Id Không tồn tại"});
                    }else{
                        if(roleAdmin.role == 1){
                            UpdateAdmin.fullName = AdminFullName;
                            UpdateAdmin.role = AdminRole;
                            UpdateAdmin.status = AdminStatus;
                            await Admin.findOneAndUpdate({_id: UpdateAdminId}, UpdateAdmin);
                            res.json({error:false,message:"Thay đổi thông tin thành công"});
                        }else{
                            res.json({error:true,message:"Bạn không có quyền thực hiện chức năng này"});
                        }
                    }
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }
    //xem tất cả danh sách admin
    async getAllAdmin(req, res) {
    try {
        const admin = await Admin.find({});
        if(admin){
            res.json({
                error: false,
                message: 'Danh Sách Tất Cả Admin',
                data: admin,
            })
        } else{
            res.json({
                error: true,
                message: 'Danh Sách Trống',
            })
        }
    } catch (error) {
       console.log(error.message)
    }
    // xem 1 admin theo username
}
    async getAdminByUsername(req,res){
        try {
            const adminUsername = req.query.adminUsername.trim();
            if(!adminUsername){
              res.json({
                  error: true,
                  message: 'Vui lòng nhập Username',
              })
            } else{
                const admin = await Admin.findOne({
                    userName: adminUsername,
                });
                if(admin){
                    res.json({
                        error: false,
                        message: `Admin ${admin.userName}`,
                        data: admin,
                    });
                } else{
                    res.json({
                        error: true,
                        message:'Admin không tồn tại',
                    });
                }
            }
        } catch (error) {
            console.log(error.message)
        }
    }
    // Đăng nhập
    async loginAdmin(req, res){
        try {
            const adminUser = req.body.userName.trim();
            const adminPassword = req.body.password.trim();
            if(!adminUser || !adminPassword){
                res.json({
                    error: true,
                    message: 'Vui lòng điền đầy đủ thông tin',
                });
            } else{
                const admin = await Admin.findOne({
                    userName: adminUser,
                });
                if(admin && admin.password === adminPassword){
                    res.json({
                        error: false,
                        message: 'Đăng Nhập Thành Công',
                        data: admin,
                    });
                } else{
                    res.json({
                        error: true,
                        message:'Vui lòng kiểm tra  tài khoản mật khẩu',
                    });
                }
            }
        } catch (error) {
            console.log(error.message)
        }
    }
}

module.exports = new AdminController;



//Doc Mongoose Nodejs
// https://mongoosejs.com/docs/
// Res.json

//[GET]
//xem tất cả danh sách admin

// xem 1 admin theo id


// Đăng nhập

// Task Phong
// Check dấu cách bằng trim()
// Chuyển dữ liệu từ param thành body
// Check body truyền lên xem có undifined hay ko
// truy vấn về kiểm tra xem có tồn tại hay ko 


//task Hải
// sửa hàm đăng nhập thành post thanh vì get
// Dòng 157 Hải truyền đúng tên biến userName trong admin thì nó mới hiện đúng tên, hiện tại sai nên bị undefined