const Product = require('../models/Product');
const Category = require('../models/Category');
const Provider = require('../models/Provider');
const {
    getFileName
} = require('../../util/getFileNameFromLink');
const {
    deleteProductImage
} = require('../../util/deleteFile');

class ProductController {

    //[GET] /api/product/getAllProduct
    async getAllProduct(req, res) {
        try {
            const product = await Product.find({});
            if (product) {
                res.json({
                    error: false,
                    message: 'Danh sách tất cả các sản phẩm',
                    data: product
                });
            } else {
                res.json({
                    error: true,
                    message: 'Không thể lấy danh sách sản phẩm',
                });
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/product/getProductByCategoryID
    async getProductByCategoryID(req, res) {
        try {
            const categoryID = req.query.categoryId.trim();
            // console.log(categoryID);
            if (!categoryID) {
                return res.json({
                    error: true,
                    messenger: "vui lòng điền ID thể loại"
                })
            } else {
                const product = await Product.find({
                    'category._id': categoryID
                });
                return res.json({
                    error: false,
                    messenger: "",
                    data: product
                })
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/product/getProductByProviderID
    async getProductByProviderID(req, res) {
        try {
            const providerID = req.query.providerId.trim();
            if (!providerID) {
                res.json({
                    error: true,
                    messenger: "vui lòng điện ID nhà cung cấp"
                })
            } else {
                const product = await Product.find({
                    'provider._id': providerID
                });
                res.json({
                    error: false,
                    messenger: "",
                    data: product
                })

            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /api/product/addProduct
    async addProduct(req, res) {
        try {

            const productName = req.body.productName.trim();
            const imageProduct = req.file.filename
            const price = req.body.price;
            const categoryId = req.body.category.trim();
            const providerId = req.body.provider.trim();

            // console.log(imageProduct);
            // console.log(productName + price + categoryId + providerId);
            // console.log(typeof price);

            // if(!productName ||!price || typeof price !== 'number' ||!categoryId || !providerId || !imageProduct) {
            if (!productName || !price || !categoryId || !providerId || !imageProduct || !imageProduct) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin"
                })
            } else {
                // console.log('path: ' + req.files[0].filename);

                const category = await Category.findOne({
                    _id: categoryId
                });
                const provider = await Provider.findOne({
                    _id: providerId
                });

                // console.log(category);
                // console.log(provider);

                if (!category) {
                    res.json({
                        error: true,
                        message: "ID thể loại không đúng"
                    });
                } else if (!provider) {
                    res.json({
                        error: true,
                        message: "ID nhà cung cấp không đúng"
                    });
                } else {
                    req.body.category = category;
                    req.body.provider = provider;
                    req.body.imageProduct = req.protocol + '://' + req.headers.host + '/image/product/' + req.file.filename;
                    req.body.status = false;

                    const newProduct = req.body;
                    const product = new Product(newProduct);

                    await product.save();
                    res.json({
                        error: false,
                        message: "",
                        data: product
                    })
                }
            }

        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /api/product/addSizeProduct
    async addDetailsProduct(req, res) {
        try {
            const productId = req.body._id.trim();
            const size = req.body.size;
            const quantityInStock = req.body.quantityInStock;
            if (!productId || !size || !quantityInStock ) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin"
                })
            } else {
                const product = await Product.findById({
                    '_id': productId
                });
                if (!product) {
                    res.json({
                        error: true,
                        message: "Sản phẩm không tồn tại"
                    })
                } else {
                    var detailSize = {
                        "size": size,
                        "quantityInStock": quantityInStock
                    }
                    product.details.push(detailSize);
                    await product.save();
                    res.json({
                        error: false,
                        message: "Thêm size thành công"
                    })
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[PUT] /api/product/updateProduct
    async updateProduct(req, res) {
        try {
            // console.log(req.body);

            const productId = req.body._id.trim();
            const productName = req.body.productName.trim();
            const price = req.body.price;
            const categoryId = req.body.categoryId.trim();
            const providerId = req.body.providerId.trim(); 
            const status = req.body.status;

            // console.log(productId + '|' + productName + '|' + price + '|' + categoryId + '|' + providerId + '|' + status);

            // if(!productId || !productName ||!price || typeof price != Number ||!categoryId || !providerId || !status) {
            if (!productId || !productName || !price || !categoryId || !providerId) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin"
                })
            } else {
                var product = await Product.findOne({
                    _id: productId
                });
                // console.log(product);
                if (product) {
                    const category = await Category.findOne({
                        _id: categoryId
                    });
                    const provider = await Provider.findOne({
                        _id: providerId
                    });

                    req.body.category = category;
                    req.body.provider = provider;

                    const updateProduct = req.body;
                    product = await Product.findByIdAndUpdate({
                        _id: updateProduct._id
                    }, updateProduct, {
                        new: true
                    });
                    res.json({
                        error: false,
                        message: "Sửa thông tin sản phẩm thành công",
                        data: product
                    })
                } else {
                    res.json({
                        error: false,
                        message: "Sản phẩm không tồn tại"
                    })
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[PUT] /api/product/updateImageProduct
    async updateImageProduct(req, res) {
        try {
            // console.log("body",req.file);
            const productId = req.body._id.trim();
            const productImage = req.file.filename;
            // console.log(productId + productImage);
            if (!productId, !productImage) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin"
                })
            } else {
                req.body.imageProduct = req.protocol + '://' + req.headers.host + '/image/product/' + req.file.filename;

                const product = await Product.findById(productId);
                if (product) {
                    const imageProduct = getFileName(product.imageProduct);
                    // console.log(product.imageProduct);
                    // console.log(imageProduct);
                    
                    product.imageProduct = req.body.imageProduct;
                    await product.save(function (error) {
                        // deleteProductImage(imageProduct);

                        if (!error) {
                            res.json({
                                error: false,
                                message: "sửa hình thành công",
                                data: product
                            })
                        } else {
                            res.json({
                                error: true,
                                message: "sửa hình thất bại",
                            })
                        }
                    })
                } else {
                    res.json({
                        error: true,
                        message: "Vui long điền đúng ID Sản phẩm",
                    })
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/product/
    async index(req, res) {
        res.json({
            error: true,
            messenger: "nothing happend"
        })
    }

    // //[GET] api/product/lu
    // async hamlu(req, res) {
    //     const productId = req.body.productId;
    //     const product = await Product.findOne({'_id':productId});
    //     console.log(product.details.length);
    //     for(var i = 0; i < product.details.length; i++) {
    //         if(product.details[i].size === 37) {
    //             product.details[i].quantityInStock -= 10 ;
    //         }
    //     }
    //     product.save();
    //     res.json({
    //         error: true,
    //         message: "",
    //         data: product
    //     });
    // }
}

module.exports = new ProductController;