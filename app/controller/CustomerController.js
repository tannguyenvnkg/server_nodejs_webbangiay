const Customer = require('../models/Customer');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const mail = require('../../util/sendEmail');

dotenv.config();

class CustomerController {

    //[GET] /customer/getAllCustomer
    async getAllCustomer(req, res) {
        try {
            const customer = await Customer.find({});
            res.json({
                error: false,
                message: 'Danh sách tất cả các khách hàng',
                data: customer
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /customer/getCustomerByEmail
    async getCustomerByEmail(req, res) {
        try {
            const customerEmail = req.query.customerEmail.trim();
            if (!customerEmail) {
                res.json({
                    error: true,
                    messenger: 'Vui lòng điền Email'
                });
            } 

            const customer = await Customer.findOne({'email': customerEmail});
            if (!customer) {
                res.json({
                    error: true,
                    messenger: 'Khách hàng không tồn tại',
                });
            }
            
            res.json({
                error: false,
                messenger: `Khách hàng ${customer.customerName}`,
                data: customer
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /customer/loginCustomer
    async login(req, res) {
        try {
            const customerEmail = req.body.email.trim();
            const customerPassword = req.body.password.trim();
            // console.log(customerEmail + customerPassword);
            if (!customerEmail || !customerPassword) {
                res.json({
                    error: true,
                    messenger: 'Vui lòng điền đầy đủ thông tin'
                });
            } 
            
            const customer = await Customer.findOne({'email': customerEmail});
            if (customer && customer.password === customerPassword) {
                const accessToken = jwt.sign({
                    data: customer
                }, process.env.ACCESS_TOKEN_SECRET_CLIENT, {
                    expiresIn: '3600s',
                });
                res.json({
                    error: false,
                    messenger: 'Đăng nhập thành công',
                    token: accessToken,
                    data: customer
                });
            } 

            res.json({
                error: true,
                messenger: 'Vui lòng kiểm tra lại tài khoản mật khẩu'
            });
            
        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /api/customer/registerCustomer
    async register(req, res) {
        try {
            const customerName = req.body.customerName.trim();
            const customerPhone = req.body.phone.trim();
            const customerBirthday = req.body.birthday.trim();
            const customerAddress = req.body.address.trim();
            const customerEmail = req.body.email.trim();
            const customerPassword = req.body.password.trim();

            if (!customerName || !customerPhone || !customerAddress || !customerEmail || !customerPassword || !customerBirthday) {
                res.json({
                    error: true,
                    messenger: 'Vui lòng điền đầy đủ thông tin'
                });
            } 
            
            const email = await Customer.findOne({email: customerEmail});
            if (email) {
                res.json({
                    error: true,
                    messenger: 'Email đã tồn tại'
                });
            }

            const newCustomer = req.body;
            const customer = new Customer(newCustomer);
            await customer.save();
            res.json({
                error: false,
                messenger: 'Đăng ký thành công',
            });

        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /api/customer/receiveOTP
    async receiveOTP(req, res) {
        try {
            const email = req.body.email.trim();
            if (!email) {
                res.json({
                    error: true,
                    message: 'Vui lòng điền Email'
                });
            } 

            const resetCode = Math.floor(10000 + Math.random() * 90000);
            const customer = await Customer.findOne({'email': email});

            if (!customer) {
                res.json({
                    error: true,
                    message: 'Email không tồn tại'
                });
            }

            customer.resetCode = resetCode;

            await customer.save();
            mail.sendEmailResetPassword(email, resetCode);

            res.json({
                error: false,
                message: 'Chúng tôi đã gửi mã xác nhận về Email của bạn, vui lòng kiểm tra hòm thư hoặc nếu bạn không tìm thấy tin nhắn hãy kiếm tra thư rác',
            });
            
        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /api/customer/checkResetCode
    async checkResetCode(req, res) {
        try {
            const email = req.body.email.trim();
            const resetCode = req.body.resetCode.trim();
            if (!email || !resetCode) {
                res.json({
                    error: true,
                    message: 'Vui lòng điền đầy đủ thông tin'
                });
            }

            const customer = await Customer.findOne({'email': email});
            if (!customer) {
                res.json({
                    error: true,
                    message: 'Email không tồn tại'
                });
            }

            if (resetCode !== customer.resetCode) {
                res.json({
                    error: true,
                    message: 'Mã xác thực không đúng'
                });
            }

            res.json({
                error: false,
                message: 'Xác thực thành công'
            });

        } catch (err) {
            console.log(err.message)
        }
    }

    //[PUT] /api/customer/resetPassword
    async resetPassword(req, res) {
        try {
            const email = req.body.email.trim();
            const resetCode = req.body.resetCode.trim();
            const newPassowrd = req.body.newPassword;
            if (!email || !resetCode || !newPassowrd) {
                res.json({
                    error: true,
                    message: 'Vui lòng điền đầy đủ thông tin'
                });
            } 

            const customer = await Customer.findOne({'email': email});
            if (!customer) {
                res.json({
                    error: true,
                    message: 'Email không tồn tại'
                });
            }

            if (customer.email == email && customer.resetCode == resetCode) {
                customer.password = newPassowrd;
                customer.resetCode = undefined;
                await customer.save();

                res.json({
                    error: false,
                    message: 'Thay đổi mật khẩu thành công'
                });
            } 

            res.json({
                error: true,
                message: 'reset code không đúng'
            });
             
        } catch (err) {
            console.log(err.message);
        }
    }

// [PUT] Sửa thông tin khách hàng
    async updateCustomer(req, res){
       try {
           const customerID = req.body._id.trim();
           const CustomerName = req.body.customerName.trim();
           const customerPhone = req.body.phone.trim();
           const customerAddress = req.body.address.trim();
           const customerBirthday = req.body.birthday.trim(); 
        //    console.log(customerID, CustomerName, customerPhone, customerAddress, customerBirthday);
           if(!customerID || !CustomerName || !customerPhone || !customerAddress || !customerBirthday){
               res.json({
                   error: true,
                   message: 'Vui lòng điền đầy đủ thông tin'
               });
            }
            
            const customer = await Customer.findOne({'_id': customerID});
            if(customer){
                customer.customerName = CustomerName,
                customer.phone = customerPhone,
                customer.address =  customerAddress,
                customer.birthday = customerBirthday

                await customer.save();
                res.json({
                    error: false,
                    message: 'Thay đổi thành công'     
                });   
            }
            
            res.json({
                error: true,
                mesage: 'Thay đổi không thành công'
            })
            
       } catch (error) {
            console.log(error.mesage)   
       }
    }

//    [PUT] Thay đổi mật khẩu
    async changePassword(req, res){
       try {
           const customerID = req.body._id.trim();
           const oldPassword = req.body.oldPassword.trim();
           const newPassword =req.body.newPassword.trim();
        //    console.log(customerID, oldPassword, newPassword);
           if(!customerID || !oldPassword || !newPassword){
                res.json({
                    error: true,
                    message: 'Vui lòng điền đầy đủ thông tin'
                });
            } 
           
            const customer = await Customer.findOne({'_id': customerID});
            if(!customer) {
                res.json({
                    error: true,
                    message:'Người dùng không tồn tại'
                });
            } 
            
            if(customer.password === oldPassword){
                customer.password = newPassword;
                await customer.save();
                res.json({
                    error: false,
                    message:'Thay đổi mật khẩu thành công'
                });
            }

            res.json({
                error: true,
                message: 'người dùng hoặc mật khẩu không đúng'
            });
            
        } catch (error) {
            console.log(error.mesage);
        }
    }

    //[GET] /customer/
    async index(req, res) {
        res.json({
            error: true,
            messenger: 'nothing happend'
        });
    }
}

module.exports = new CustomerController;

//task hải
// [PUT] Sửa thông tin khách hàng, 
//check những thông tin truyền vào có đúng ko
// kiem tra xem khách hàng có tồn tại ko
// sửa thông tin 
// Lưu database xuất lại thông tin

// cost _id
// const customerName = req.body.customerName.trim();
// const customerPhone = req.body.phone.trim();
// const customerBirthday = req.body.birthDay.trim();
// const customerAddress = req.body.address.trim();
// const customerEmail = req.body.email.trim();

// [PUT] Thay đổi mật khẩu

//body _id, oldPassword và newPassword
// So sánh old password có giống trong database hay ko
// Nếu giống đổi password cũa thành password mới
// ko giống json trả về ko đúng

// {
//     "_id": "23653246346",
//     "oldPassword" : "haicho123",
//     "newPassword" : "haicho123456"
// }