const Product = require('../models/Product');
const Provider = require('../models/Provider');

class ProviderController {

    //[GET] /provider/getAllProvider
    async getAllProvider(req, res) {
        try {
            const provider = await Provider.find({});
            res.json({
                error: false,
                message: 'Danh sách tất cả các nhà cung cấp',
                data: provider
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /provider/addProvider
    async addProvider(req, res) {
        try {
            const providerName = req.body.providerName.trim();
            const providerAddress = req.body.address.trim();
            const providerPhone = req.body.phone.trim();
            if (!providerName || !providerAddress || !providerPhone) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin"
                })
            } else {
                const newProvider = req.body;
                const provider = new Provider(newProvider);
                await provider.save();
                res.json({
                    error: false,
                    message: `Đã thêm nhà cung cấp ${provider.providerName} thành công`
                })
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[PUT] /provider/updateProvider
    async updateProvider(req, res) {
        try {
            const providerId = req.body._id.trim();
            const providerName = req.body.providerName.trim();
            const providerAddress = req.body.address.trim();
            const providerPhone = req.body.phone;
            if (!providerId || !providerName || !providerAddress || !providerPhone) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đúng thông tin"
                })
            } else {
                let provider = await Provider.findOne({
                    _id: providerId
                });
                // console.log(provider);
                if (provider) {
                    const updateProvider = req.body;
                    provider = await Provider.findOneAndUpdate({
                        _id: updateProvider._id
                    }, updateProvider, {
                        new: true
                    });
                    
                    await Product.updateMany({'provider._id': providerId}, {"$set": {provider: updateProvider}});
                   
                    res.json({
                        error: false,
                        message: `Đã sửa nhà cung cấp ${provider.providerName} thành công `,
                    })
                } else {
                    res.json({
                        error: true,
                        message: "Nhà cung cấp không tồn tại",
                    })
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /provider/
    async index(req, res) {
        res.json({
            error: true,
            messenger: 'nothing happend'
        })
    }
}

module.exports = new ProviderController;