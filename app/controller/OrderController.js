const Order = require('../models/Order');
const Customer = require('../models/Customer');
const Product = require('../models/Product');
const RemoveNewObjectId = require("../../util/RemoveNewObjectID");
const moment = require('moment');

class OrderController {

    //[GET] /api/order/getAllOrder
    async getAllOrder(req, res) {
        try {
            const order = await Order.find({});
            res.json({
                error: false,
                message: "Danh sách tất cả các đơn hàng",
                data: order
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/order/getOrderById
    async getOrderById(req, res) {
        try {
            const orderId = req.query.orderId.trim();
            if (!orderId) {
                res.json({
                    error: true,
                    message: "Vui lòng điền mã đơn hàng"
                })
            } else {
                const order = await Order.findById({
                    '_id': orderId
                });
                if (order) {
                    res.json({
                        error: false,
                        message: "Đơn hàng",
                        data: order
                    })
                } else {
                    res.json({
                        error: true,
                        message: "Đơn hàng không tồn tại"
                    })
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/order/getOrderByCustomerId
    async getOrderByCustomerEmail(req, res) {
        try {
            const customerEmail = req.query.customerEmail.trim();
            if (!customerEmail) {
                res.json({
                    error: true,
                    message: "Vui lòng điền mã đơn hàng"
                })
            } else {
                const order = await Order.find({'emailCustomer': customerEmail});
                res.json({
                    error: false,
                    message: "Đơn hàng của khách hàng ",
                    data: order
                })
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/order/getOrderBeingDelivered
    async getOrderBeingDelivered(req, res) {
        try {
            const order = await Order.find({'deliveryStatus': undefined});
            res.json({
                error: false,
                message: "Danh sách đơn hàng đang giao",
                data: order
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/order/getOrderDeliverySuccess
    async getOrderDeliverySuccess(req, res) {
        try {
            const order = await Order.find({'deliveryStatus': true});
            res.json({
                error: false,
                message: "Danh sách đơn hàng giao thành công",
                data: order
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[GET] /api/order/getOrderDeliveryCancel
    async getOrderDeliveryCancel(req, res) {
        try {
            const order = await Order.find({'deliveryStatus': false});
            res.json({
                error: false,
                message: "Danh sách đơn hàng đã hủy",
                data: order
            });
        } catch (err) {
            console.log(err.message);
        }
    }

    //[POST] /api/order/addOrder
    async checkOrder(req, res, next) {
        try {
            const cart = req.body.cart;
            for (var i = 0; i < cart.length; i++) {
                const product = await Product.findOne({
                    '_id': cart[i].productId
                });
                // console.log(product);
                for (var j = 0; j < product.details.length; j++) {
                    // console.log(product.details[j]._id.toString());
                    const removeObjetId = RemoveNewObjectId.removeNewObjectID(product.details[j].sizeId.toString())
                    // console.log(removeObjetId);
                    // console.log(cart[i].sizeId === removeObjetId);
                    if (cart[i].sizeId === removeObjetId) {
                        // console.log(product.details[j].quantityInStock);
                        if (product.details[j].quantityInStock === 0) {

                            return res.json({
                                error: true,
                                message: `Size ${product.details[j].size} của ${product.productName} đã hết hàng`,
                            })
                        }
                        if (cart[i].amount > product.details[j].quantityInStock) {
                            return res.json({
                                error: true,
                                message: `Size ${product.details[j].size} của ${product.productName} không có đủ số lượng`,
                            })
                        }
                    }
                }
            }
            next();
        } catch (err) {
            console.log(err);
        }
    }

    //[POST] /api/order/addOrder
    async addOrder(req, res) {
        try {
            const nameCustomer = req.body.nameCustomer.trim();
            const emailCustomer = req.body.emailCustomer.trim();
            const phoneCustomer = req.body.phoneCustomer.trim();
            const addressCustomer = req.body.addressCustomer.trim();
            const cart = req.body.cart;
            // console.log(req.body);
            if (!nameCustomer || !emailCustomer || !phoneCustomer || cart.length === 0 || !addressCustomer) {
                res.json({
                    error: true,
                    message: "Vui lòng điền đầy đủ thông tin",
                });
            } else {
                var detail = [];
                var total = 0;
                for (var i = 0; i < cart.length; i++) {
                    // console.log(cart.length);
                    const product = await Product.findOne({
                        '_id': cart[i].productId
                    });
                    // console.log(product);
                    for (var j = 0; j < product.details.length; j++) {
                        // console.log(cart[i].sizeId);
                        // console.log(product.details[j]._id);
                        const removeObjetId = RemoveNewObjectId.removeNewObjectID(product.details[j].sizeId.toString())

                        if (cart[i].sizeId === removeObjetId) {
                            product.details[j].quantityInStock -= cart[i].amount; // chỗ này lú nha
                            var newDetail = {
                                "sideId": product.details[j].sizeId,
                                "size": product.details[j].size,
                                "amount": cart[i].amount
                            }
                            break;
                        }
                    }
                    await product.save();
                    // console.log(newDetail)
                    product.details = undefined;
                    product.detail = newDetail;
                    // console.log(product.details);
                    detail.push(product);
                    total += (product.price * cart[i].amount);
                }
                // console.log(detail);
                var today = new Date();
                var now = moment(today).format('YYYY-MM-DD');
                const newOrder = {
                    "nameCustomer": nameCustomer,
                    "emailCustomer": emailCustomer,
                    "phoneCustomer": phoneCustomer,
                    "paid": false,
                    "deliveryStatus": undefined,
                    "orderDate": now,
                    "deliveryDate": undefined,
                    "addressCustomer": addressCustomer,
                    "totalMoney": total,
                    "cart": detail
                }
                const order = new Order(newOrder);
                await order.save();
                res.json({
                    error: false,
                    message: "Đặt hàng thành công"
                })
            }
        } catch (err) {
            console.log(err.message);
        }
    }


    //[PUT] /api/order/deliverySuccess
    async deliverySuccess(req, res) {
        try {
            const orderId = req.body.orderId.trim();
            if (!orderId) {
                res.json({
                    error: true,
                    message: "Vui lòng điền ID đơn hàng",
                });
            } else {
                const order = await Order.findById({
                    '_id': orderId
                });
                if (order) {
                    var today = new Date();
                    var now = moment(today).format('YYYY-MM-DD');
                    order.paid = true;
                    order.deliveryStatus = true;
                    order.deliveryDate = now;

                    await order.save();
                    res.json({
                        error: false,
                        message: "Xác nhận đơn hàng thành công",
                    });
                } else {
                    res.json({
                        error: true,
                        message: "Đơn hàng đã được giao thành công",
                    });
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[PUT] /api/order/deliveryCancel
    async deliveryCancel(req, res) {
        try {
            const orderId = req.body.orderId.trim();
            if (!orderId) {
                res.json({
                    error: true,
                    message: "Vui lòng điền ID đơn hàng",
                });
            } else {
                const order = await Order.findById({
                    '_id': orderId
                });
                if (order) {
                    var today = new Date();
                    var now = moment(today).format('YYYY-MM-DD');
                    order.paid = false;
                    order.deliveryStatus = false;
                    order.deliveryDate = now;

                    await order.save();
                    res.json({
                        error: false,
                        message: "Đơn hàng đã được hủy",
                    });
                } else {
                    res.json({
                        error: true,
                        message: "Đơn hàng không tồn tại",
                    });
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //[get] /api/order/
    async index(req, res) {
        res.json({
            error: true,
            message: "nothing",
        });
    }
}

module.exports = new OrderController;