const pathProductImage = require('./path');
var fs = require('fs');

function deleteFile(path){
    fs.stat(path, function (err, stats) {
        console.log(stats);
        if (err) {
            return console.error(err);
        }
        fs.unlink(path,function(err){
             if(err) return console.log(err);
             console.log('file deleted successfully');
        });  
     });
}

function deleteProductImage(fileName) {
    const path = pathProductImage.pathProductImage + fileName;
    console.log(pathProductImage.pathProductImage);
    deleteFile(path);
}

module.exports = {
    deleteProductImage
}