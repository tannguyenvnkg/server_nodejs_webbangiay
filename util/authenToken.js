const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

function authenTokenClient(req, res, next) {
    const authorizationHeader = req.headers['authorization'];
    // 'Beaer [token]'
    const token = authorizationHeader.split(' ')[1];
    if (!token) {
        // res.sendStatus(401).json({
        //     error : true,
        //     messenger: "không có token"
        // });
        return res.status(401).json({
            error: true,
            message: "không có token"
        });
    } else {
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET_CLIENT, function (error, data) {
            if (error) {
                return res.status(403).json({
                    error: true,
                    message: "không có quyền"
                })
            }
        });
    }
    next();
}

module.exports = {
    authenTokenClient
}